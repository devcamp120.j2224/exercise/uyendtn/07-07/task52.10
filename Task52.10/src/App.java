import com.devcamp.j52_javabasic.s10.Person;
import java.util.*;


public class App {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        //tạo arraylist person
        ArrayList<Person> myPerson = new ArrayList<>();
        //tạo person1
        Person myPerson1 = new Person();
        //tạo person2
        Person myPerson2 = new Person("huong");
        //tạo person 3
        Person myPerson3 = new Person("John", 20);
        //tạo person4
        Person myPerson4 = new Person("john", 30, 50, 1000, new String[] {"cat","bird"});
        //add từng person vào list
        myPerson.add(myPerson1);
        myPerson.add(myPerson2);
        myPerson.add(myPerson3);
        myPerson.add(myPerson4);
        //in ra
        for (Person person:myPerson){
            System.out.println(person.toString());
        }
        

    }
}
