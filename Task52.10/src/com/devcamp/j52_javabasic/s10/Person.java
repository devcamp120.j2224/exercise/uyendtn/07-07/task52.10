package com.devcamp.j52_javabasic.s10;

public class Person {
    public String name;
    public int age;
    public double weight;
    public long salary;
    public String[] pets;
    //khởi tạo constructor thứ nhất không có tham số
    public Person(){
        this.name = "Uyen";
        this.age = 30;
        this.weight = 50;
        this.salary = 5000000;
        this.pets = new String[] {"dog","cat","goose"};
    }
    public String toString() {
        return "Person = [name: " + name + " age: " + age + " weight: " + weight 
                        + " salary: " + salary + " pets: "
                        + pets[0] +", " + pets[1]+", " + pets[2] + "]" ;
        
    }
    //khơi tạo constructor thứ hai có 1 tham số
    public Person(String myName){
        this.name = myName;
        this.age = 30;
        this.weight = 50;
        this.salary = 5000000;
        this.pets = new String[] {"dog","cat","goose"};
    }
    //khỏi tạo constructor thứ ba có 2 tham số
    public Person(String myName, int myAge){
        this.name = myName;
        this.age = myAge;
        this.weight = 50;
        this.salary = 5000000;
        this.pets = new String[] {"dog","cat","goose"};
    }
    //khởi tạo constructor thứ tư có đầy đủ tham số
    public Person(String myName, int myAge, double myWeight, long mySalary, String[] myPets){
        this.name = myName;
        this.age = myAge;
        this.weight = myWeight;
        this.salary = mySalary;
        this.pets = myPets;
    }
}
